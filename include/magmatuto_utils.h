#ifndef MAGMATUTO_UTILS_H
#define MAGMATUTO_UTILS_H

enum magmatuto_out_ {
	MT_SUCCESS = 0,
	MT_FAILURE = 1,
	MT_NULL_POINTER = 2,
	MT_MALLOC_FAILURE = 3,
	MT_FILE_ACESS_FAILURE = 4
};
typedef enum magmatuto_out_ mt_out;

/* Abort the program execution */
void magmatuto_abort(char const* const s);
void magmatuto_return(char const* const func, int const line, char const* const file);
void magmatuto_error(char const* const message, char const* const func, int const line, char const* const file);

#define mt_check_abort_error(mt_val, message)\
	{\
		if (mt_val != MT_SUCCESS) {\
			magmatuto_error(message, __FUNCTION__, __LINE__, __FILE__);\
			exit(mt_val);\
		}\
	}

#define mt_check_abort(mt_val)\
	{\
		if (mt_val != MT_SUCCESS) {\
			magmatuto_return(__FUNCTION__, __LINE__, __FILE__);\
			exit(mt_val);\
		}\
	}

#define mt_check_return_error(mt_val, message)\
	{\
		if (mt_val != MT_SUCCESS) { \
			magmatuto_error(message, __FUNCTION__, __LINE__, __FILE__);\
			return mt_val;\
		}\
	}

#define mt_check_return(mt_val)\
	{\
		if (mt_val != MT_SUCCESS) { \
			magmatuto_return(__FUNCTION__, __LINE__, __FILE__);\
			return mt_val;\
		}\
	}

/* Memory allocation processes */
mt_out magmatuto_malloc(void** const p, size_t const n, size_t const size);
mt_out magmatuto_calloc(void** const p, size_t const n, size_t const size);
mt_out magmatuto_free(void** const p);

#define mt_malloc(p, n, size) magmatuto_malloc((void**)p, n, size)
#define mt_calloc(p, n, size) magmatuto_calloc((void**)p, n, size)
#define mt_free(p) magmatuto_free((void**)p)

/* Lapack functions */
extern void dgemm(char *transa, char *transb, int *m, int *n, int *k,
	double *alpha, double *a, int *lda, double *b, int *ldb,
	double *beta, double *c, int *ldc);
extern void dgetrf( int *m, int *n, double *a, int *lda, int *ipiv, int *info );
extern void dgetrs( char *trans, int *n, int *nrhs,
	double *a, int *lda, int *ipiv,
	double *b, int *ldb, int *info );

/*norm 1 or infinite of a matrice, type='1' or 'I' */
double magmatuto_norm(char const type, int const M, int const N, double const* const A, int const LDA);

/*norm 2 of a matrix*/
double magmatuto_norm2(int const M, int const N, double const* const A, int const LDA);

/* Print a matrix */
void magmatuto_printMat(int const M, int const N, double const* const A, int const LDA,
	char const* const desc);

/* Compute the residual eps: machine precision, norm1 = norm(b-AX), norm2 = norm(b-AX)/(eps*norm(A)*norm(X)*n) */
void magmatuto_residual(int m, int n, int nrhs, double *A, int LDA, double *B, int LDB, double *X, int LDX,  double *eps, double *resid1, double *resid2);

/* Print a progress step message */
void magmatuto_stepPrint(char const* const s);

/* Get the current time of the dat in microseconds */
long magmatuto_usecs();

/* Write a data in a file */
mt_out magmatuto_write(char const* const filename, char const* const data);
#endif
