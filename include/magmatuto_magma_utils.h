#ifndef MAGMATUTO_MAGMA_UTILS_H
#define MAGMATUTO_MAGMA_UTILS_H

#include "magma_v2.h"

/* Check the error for a routine called by MAGMA */
void magmatuto_magma_checkError(magma_int_t const err, char const* const s);

#endif
