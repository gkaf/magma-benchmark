/*
 *	Author: Simplice Donfack
 *
 *  Purpose
 *  =======
 *	Tutorial 5: solve the system AX=B, A being factorized using magma_dgetrf_native routine
 *
 *  Usage
 *  =====
 *  ./tutorial5_magma_dgetrf_native <m> <nrhs>
 *
 *  Input parameters:
 *  ================
 *	m:	Matrix size
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "magma_v2.h"
#include "magmatuto_utils.h"
#include "magmatuto_magma_utils.h"

int main(int argc, char *argv[])
{
   int m, n; 				 /* Matrix dimension  */
   int LDA, LDB, LDX;  	/* Leading dimension of A, B and X */
   int nrhs; 				 /* Number of right hand side */
   double *A;        /* Matrix to factorize Dimension(m,n) */
   double *AWORK;    /* Matrix workspace Dimension(m,n) */
   double *B; 			 /* Right hand side dimension(m,nrhs)*/
   double *X; 			 /* Solution Dimension(n,nrhs)*/
   int *IPIV; 			 /* The permutation vector .*/
   int INFO = 0;

   int i;
   long t1;
   double gflops, eps, resid1, resid2;
   double dZERO=0.0, dONE=1.0;  /* Parameters */
   int ts = CLOCKS_PER_SEC;
   magmaDouble_ptr dA = NULL;   /* Matrix to factorize on the GPU */
   magma_queue_t queue = NULL;  /* Default program queue */
   int LDDA;  				 	      	/* Leading dimension of dA */
   magma_int_t merr;

   //Initialize MAGMA
   merr = magma_init(); magmatuto_magma_checkError(merr, "magma_init");
   magma_print_environment();

   // Inputs
   m = n = 10000;
   nrhs = 1;

   // Get user parameters
   if(argc>1) m = atoi(argv[1]);
   if(argc>2) nrhs = atoi(argv[2]);

   n = m;
   printf("Matrix m: %d, n: %d, nrhs: %d\n", m, n, nrhs);

   // Initialisation
   LDA = m; LDB = m; LDX = n;

   //round the leading dimension up to a multiple of 32 for performance
   LDDA   = magma_roundup( m, 32 );  // multiple of 32

   // Set the seed for random numbers
   srand (time (NULL)); // For random numbers

   //mkl_set_num_threads(P);

   // Allocate memory
   if(!(A     =	(double *)	malloc(m*n*sizeof(double))))    magmatuto_abort("Memory allocation failed for A");
   if(!(B     =	(double *)	malloc(m*nrhs*sizeof(double)))) magmatuto_abort("Memory allocation failed for B");
   if(!(X     =	(double *)	malloc(n*nrhs*sizeof(double)))) magmatuto_abort("Memory allocation failed for X");
   if(!(IPIV  = (int*)		  malloc(m*sizeof(int))))         magmatuto_abort("Memory allocation failed for IPIV");
   if(!(AWORK = (double *)	malloc(m*n*sizeof(double))))    magmatuto_abort("Memory allocation failed for AWORK");

   //Allocate memory for the GPU
   merr = magma_dmalloc( &dA,  LDDA*n); magmatuto_magma_checkError(merr, "magma_dmalloc for dA");

   /*
    * Generating the problem, assuming the matrix is stored column by column
    */

   // Generate A
   magmatuto_stepPrint("Generating A");
   for(i=0;i<m*n;i++) {
     A[i] = (double) rand()/RAND_MAX ;
   }

   // Generate X
   for(i=0;i<n*nrhs;i++) {
     X[i] = 1.0;
   }

   // Save a copy of A
   for(i=0;i<m*n;i++) {
     AWORK[i] = A[i];
   }

   // Compute  B = A*X  and store in B.
   magmatuto_stepPrint("Computing B");

   dgemm("N","N",&m,&nrhs,&n,&dONE,A,&LDA,X,&LDX,&dZERO,B,&LDB); /* B = 1*A*X + 0*B */

   // Set gflops = billions of floating point operations that will be performed
   gflops = 1.0 * (m - n/3)* n * n * 1.0e-09;



   /*
    *  Compute the LU factorization of A using MAGMA_DGETRF.
    */

    //Create a queue on the default device
 	 magma_int_t device = 0;
 	 magma_getdevice( &device );
 	 magma_queue_create(device, &queue);

    // Copy the matrix on the GPU
 	 magmatuto_stepPrint("Copying the matrix to the GPU");
 	 magma_dsetmatrix( m, n, A, LDA, dA, LDDA, queue);

   magmatuto_stepPrint("Factorising using DGETRF");

   t1 = magmatuto_usecs();
   magma_dgetrf_native(m, n, dA, LDDA, IPIV, &INFO);
   t1 = magmatuto_usecs()-t1;

   printf("MAGMA_DGETRF_NATIVE time: %.2f, GFlops/s: %4.2lf\n", 1.0*t1/ts, gflops*ts/t1);
   if(INFO!=0) printf("Error DGETRF returned : %d\n",INFO);

   //Get the matrix on the CPU
   magmatuto_stepPrint("Copying the matrix from the GPU");
   magma_dgetmatrix(m, n, dA, LDDA, A, LDA, queue);

   /*
    *  Solve the system AX = B.
    */

   magmatuto_stepPrint("Solving");

   // Save B
   for(i=0;i<m*nrhs;i++) {
     X[i] = B[i] ;
   }

   // Solve
   dgetrs("N", &n, &nrhs, A, &LDA, IPIV, X, &LDX, &INFO);
   if(INFO!=0) printf("Error DGETRS returned : %d\n",INFO);

   // Compute residual norms: Resid1 = norm(B - A*X), Resid2 = norm(B - A*X) / ( n * norm(A) * norm(X) * EPSILON ) */
   magmatuto_stepPrint("Computing the residual");
   magmatuto_residual(m, n, nrhs, AWORK, LDA, B, LDB, X, LDX, &eps, &resid1, &resid2);
   printf("Machine precision: %e, Residual1:%e, Residual2: %e\n", eps, resid1, resid2);

   //Free memory
   magmatuto_stepPrint("Free memory");
   free(A);free(B);free(X);
   free(AWORK);free(IPIV);

   //free GPU memory and destroy the default queue
   magma_queue_destroy( queue );
   magma_free( dA );

   //Finalize MAGMA
   magma_finalize();

   return 0;
}
