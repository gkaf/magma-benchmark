/*
 *	Author: Simplice Donfack
 *
 *  Purpose
 *  =======
 *	Tutorial 1: solve the system AX=B, A being factorized using LAPACK dgetrf routine
 *
 *  Usage
 *  =====
 *  ./tutorial1_test_dgetrf <m> <nrhs>
 *
 *  Input parameters:
 *  ================
 *	m:	Matrix size
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "magmatuto_utils.h"

int main(int argc, char *argv[])
{
	int m, n;          /* Matrix dimension  */
	int LDA, LDB, LDX; /* Leading dimension of A, B and X */
	int nrhs;          /* Number of right hand side */
	double* A;         /* Matrix to factorize Dimension(m,n) */
	double* AWORK;     /* Matrix workspace Dimension(m,n) */
	double* B;         /* Right hand side dimension(m,nrhs)*/
	double* X;         /* Solution Dimension(n,nrhs)*/
	int* IPIV;         /* The permutation vector .*/
	int INFO = 0;

	long t1;
	double gflops, eps, resid1, resid2;
	double dZERO = 0.0, dONE = 1.0;  /* Parameters */
	int ts = CLOCKS_PER_SEC;

	// Inputs
	m = n = 10000;
	nrhs = 1;

	// Get user parameters
	if (argc>1) m = atoi(argv[1]);
	if (argc>2) nrhs = atoi(argv[2]);

	n = m;
	printf("Matrix m: %d, n: %d, nrhs: %d\n", m, n, nrhs);

	// Initialisation
	LDA = m; LDB = m; LDX = n;

	// Set the seed for random numbers
	srand(time(NULL));

	//mkl_set_num_threads(P);

	// Allocate memory
	mt_out ierr = MT_SUCCESS;
	ierr = mt_malloc(&A, m*n, sizeof(double)); mt_check_return(ierr);
	ierr = mt_malloc(&B, m*nrhs, sizeof(double)); mt_check_return(ierr);
	ierr = mt_malloc(&X, n*nrhs, sizeof(double)); mt_check_return(ierr);
	ierr = mt_malloc(&IPIV, m, sizeof(int)); mt_check_return(ierr);
	ierr = mt_malloc(&AWORK, m*n, sizeof(double)); mt_check_return(ierr);

	/*
	 * Generating the problem, assuming the matrix is stored column by column
	 */

	// Generate A
	magmatuto_stepPrint("Generating A");
	for (int i=0; i < m*n; ++i) {
		A[i] = (double) rand()/RAND_MAX ;
	}

	// Generate X
	for (int i=0; i<n*nrhs; ++i) {
		X[i] = 1.0;
	}

	// Save a copy of A
	for (int i=0; i<m*n; ++i) {
		AWORK[i] = A[i];
	}

	// Compute  B = A*X  and store in B.
	magmatuto_stepPrint("Computing B");

	dgemm("N", "N", &m, &nrhs, &n, 
		&dONE, A, &LDA, X, &LDX, &dZERO, B, &LDB); /* B = 1*A*X + 0*B */

	// Set gflops = billions of floating point operations that will be performed
	gflops = 1.0 * (m - n/3)* n * n * 1.0e-09;

	/*
	 *  Compute the LU factorization of A using LAPACK.
	 */

	magmatuto_stepPrint("Factorising using DGETRF");

	t1 = magmatuto_usecs();
	dgetrf(&m, &n, A, &LDA, IPIV, &INFO);
	t1 = magmatuto_usecs() - t1;

	printf("DGETRF time: %.2f, GFlops/s: %4.2lf\n", 1.0*t1/ts, gflops*ts/t1);
	if (INFO != 0) printf("Error DGETRF returned : %d\n", INFO);

	/*
	 *  Solve the system AX = B.
	 */
	magmatuto_stepPrint("Solving");

	// Save B
	for (int i=0; i<m*nrhs; ++i) {
		X[i] = B[i] ;
	}

	// Solve
	dgetrs("N", &n, &nrhs, A, &LDA, IPIV, X, &LDX, &INFO);
	if (INFO != 0) printf("Error DGETRS returned : %d\n",INFO);

	// Compute residual norms: Resid1 = norm(B - A*X), Resid2 = norm(B - A*X) / ( n * norm(A) * norm(X) * EPSILON ) */
	magmatuto_stepPrint("Computing the residual");
	magmatuto_residual(m, n, nrhs, AWORK, LDA, B, LDB, X, LDX, &eps, &resid1, &resid2);
	printf("Machine precision: %e, Residual1:%e, Residual2: %e\n", eps, resid1, resid2);

	//Free memory
	magmatuto_stepPrint("Free memory");
	ierr = mt_free(&A); mt_check_return(ierr);
	ierr = mt_free(&B); mt_check_return(ierr);
	ierr = mt_free(&X); mt_check_return(ierr);
	ierr = mt_free(&AWORK); mt_check_return(ierr);
	ierr = mt_free(&IPIV); mt_check_return(ierr);

	return 0;
}
