/*
 *	Author: Simplice Donfack
 *
 *  Purpose
 *  =======
 *	Tutorial 8: solve the system AX=B, A being factorized using magma_dgetrf_mgpu routine
 *
 *  Usage
 *  =====
 *  ./tutorial8_magma_dgetrf_mgpu  <m> <nrhs> <ngpus>
 *
 *  Input parameters:
 *  ================
 *	m:	Matrix size
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "magma_v2.h"
#include "magmatuto_utils.h"
#include "magmatuto_magma_utils.h"

int main(int argc, char *argv[])
{
   int m, n; 				 /* Matrix dimension  */
   int LDA, LDB, LDX;  	/* Leading dimension of A, B and X */
   int nrhs; 				 /* Number of right hand side */
   double *A;        /* Matrix to factorize Dimension(m,n) */
   double *AWORK;    /* Matrix workspace Dimension(m,n) */
   double *B; 			 /* Right hand side dimension(m,nrhs)*/
   double *X; 			 /* Solution Dimension(n,nrhs)*/
   int *IPIV; 			 /* The permutation vector .*/
   int INFO = 0;

   int i;
   long t1;
   double gflops, eps, resid1, resid2;
   double dZERO=0.0, dONE=1.0;  /* Parameters */
   int ts = CLOCKS_PER_SEC;
   magmaDouble_ptr dA[ MagmaMaxGPUs ];   /* Matrix to factorize on the GPU */
   magma_queue_t queues [ MagmaMaxGPUs ];  /* Default program queue */
   int LDDA;  				 	      	/* Leading dimension of dA */
   magma_int_t merr;
   int ngpus, nb;
   magma_int_t n_local, ldn_local;
   //Initialize MAGMA
   merr = magma_init(); magmatuto_magma_checkError(merr, "magma_init");
   magma_print_environment();

   // Inputs
   ngpus = 2;
   m = n = 10000;
   nrhs = 1;

   // Get user parameters
   if(argc>1) m = atoi(argv[1]);
   if(argc>2) nrhs = atoi(argv[2]);
   if(argc>3) ngpus = atoi(argv[3]);

   n = m;
   printf("Matrix m: %d, n: %d, nrhs: %d, ngpus:%d, max API GPUs: %d\n", m, n, nrhs, ngpus, MagmaMaxGPUs);

   if(ngpus>MagmaMaxGPUs){
   printf("The number of GPUs provided (%d) is greater than the maximun API number of GPUs: %d\n", ngpus, MagmaMaxGPUs);
   magmatuto_abort("Fix the number of GPUs");
   }

   // Initialisation
   LDA = m; LDB = m; LDX = n;

   //round the leading dimension up to a multiple of 32 for performance
   LDDA   = magma_roundup( m, 32 );  // multiple of 32

   //get the optimal block size
   nb     = magma_get_dgetrf_nb( m, n );
   printf("nb: %d\n", nb);

   // ngpu must be at least the number of blocks
   if ( ngpus > magma_ceildiv(n, nb) ) {
     ngpus = magma_ceildiv(n,nb);
     printf( "Too many GPUs for the matrix size, using %d GPUs\n", ngpus );
   }

   // Set the seed for random numbers
   srand (time (NULL)); // For random numbers

   //mkl_set_num_threads(P);

   // Allocate memory
   if(!(A     =	(double *)	malloc(m*n*sizeof(double))))    magmatuto_abort("Memory allocation failed for A");
   if(!(B     =	(double *)	malloc(m*nrhs*sizeof(double)))) magmatuto_abort("Memory allocation failed for B");
   if(!(X     =	(double *)	malloc(n*nrhs*sizeof(double)))) magmatuto_abort("Memory allocation failed for X");
   if(!(IPIV  = (int*)		  malloc(m*sizeof(int))))         magmatuto_abort("Memory allocation failed for IPIV");
   if(!(AWORK = (double *)	malloc(m*n*sizeof(double))))    magmatuto_abort("Memory allocation failed for AWORK");

   //Allocate memory for the GPU
   for(i=0; i < ngpus; i++) {
      n_local = ((n/nb)/ngpus)*nb;
      if (i < (n/nb) % ngpus)
          n_local += nb;
      else if (i == (n/nb) % ngpus)
          n_local += n % nb;
      ldn_local = magma_roundup( n_local, 32 );  // multiple of 32 by default
      magma_setdevice( i );
      merr = magma_dmalloc( &dA[i],  LDDA*ldn_local); magmatuto_magma_checkError(merr, "magma_dmalloc for dA");
   }


   /*
    * Generating the problem, assuming the matrix is stored column by column
    */

   // Generate A
   magmatuto_stepPrint("Generating A");
   for(i=0;i<m*n;i++) {
     A[i] = (double) rand()/RAND_MAX ;
   }

   // Generate X
   for(i=0;i<n*nrhs;i++) {
     X[i] = 1.0;
   }

   // Save a copy of A
   for(i=0;i<m*n;i++) {
     AWORK[i] = A[i];
   }

   // Compute  B = A*X  and store in B.
   magmatuto_stepPrint("Computing B");

   dgemm("N","N",&m,&nrhs,&n,&dONE,A,&LDA,X,&LDX,&dZERO,B,&LDB); /* B = 1*A*X + 0*B */

   // Set gflops = billions of floating point operations that will be performed
   gflops = 1.0 * (m - n/3)* n * n * 1.0e-09;



   /*
    *  Compute the LU factorization of A using MAGMA_DGETRF.
    */

    //Create queues for each devices
   for(i=0;i<ngpus;i++) {
     /* ... FILL HERE ... */
   }


    // Copy the matrix on the GPU
 	 magmatuto_stepPrint("Copying the matrix to the GPU");

   /* ... FILL HERE ... */

   magmatuto_stepPrint("Factorising using DGETRF");

   t1 = magmatuto_usecs();

   /* ... FILL HERE ... */


   t1 = magmatuto_usecs()-t1;

   printf("MAGMA_DGETRF_MGPU time: %.2f, GFlops/s: %4.2lf\n", 1.0*t1/ts, gflops*ts/t1);
   if(INFO!=0) printf("Error DGETRF returned : %d\n",INFO);

   //Get the matrix on the CPU
   magmatuto_stepPrint("Copying the matrix from the GPU");
   /* ... FILL HERE ... */

   /*
    *  Solve the system AX = B.
    */

   magmatuto_stepPrint("Solving");

   // Save B
   for(i=0;i<m*nrhs;i++) {
     X[i] = B[i] ;
   }

   // Solve
   dgetrs("N", &n, &nrhs, A, &LDA, IPIV, X, &LDX, &INFO);
   if(INFO!=0) printf("Error DGETRS returned : %d\n",INFO);

   // Compute residual norms: Resid1 = norm(B - A*X), Resid2 = norm(B - A*X) / ( n * norm(A) * norm(X) * EPSILON ) */
   magmatuto_stepPrint("Computing the residual");
   magmatuto_residual(m, n, nrhs, AWORK, LDA, B, LDB, X, LDX, &eps, &resid1, &resid2);
   printf("Machine precision: %e, Residual1:%e, Residual2: %e\n", eps, resid1, resid2);

   //Free memory
   magmatuto_stepPrint("Free memory");
   free(A);free(B);free(X);
   free(AWORK);free(IPIV);

   //free GPU memory
   for(i=0;i<ngpus;i++) {
     /* ... FILL HERE ... */
     magma_free(dA[i]);
   }

   //Destroy queues
   for(i=0;i<ngpus;i++) {
     /* ... FILL HERE ... */
   }

   //Finalize MAGMA
   magma_finalize();

   return 0;
}
