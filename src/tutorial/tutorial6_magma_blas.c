/*
 *	Author: Simplice Donfack
 *
 *  Purpose
 *  =======
 *	Tutorial 6: compute the product Y = AX using magma_dgemm
 *
 *  Usage
 *  =====
 *  ./tutorial6_magma_blas <m> <k> <n>
 *  Input parameters:
 *  ================
 *  m: number of rows of A
 *  k: number of columns of A, and the number of rows of X
 *  n: number of columns of X
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "magma_v2.h"
#include "magmatuto_utils.h"
#include "magmatuto_magma_utils.h"

int main(int argc, char *argv[])
{
   int m, k, n; 				 /* Matrices dimensions  */
   double *A, *X, *Y;    /* Matrices for the product Dimension(m,k), Dimension(k,n), Dimension(m,n)*/
   int LDA, LDX, LDY;  	 /* Leading dimension of A, Y and X */
   double *YWORK;        /* Matrix workspace Dimension(m,n) */

   int i;
   double dZERO=0.0, dONE=1.0;  /* Parameters */
   magmaDouble_ptr dA = NULL, dY = NULL, dX = NULL; /* Matrices on the device */
   magma_queue_t queue = NULL;  /* Default program queue */
   int LDDA, LDDY, LDDX;  		  /* Leading dimension of dA, dY, dX */
   magma_int_t merr;

   //Initialize MAGMA
   merr = magma_init(); magmatuto_magma_checkError(merr, "magma_init");
   magma_print_environment();

   // Default inputs
   m = k = 10000; n = 100;

   // Get user parameters
   if(argc>1) m = atoi(argv[1]);
   if(argc>2) k = atoi(argv[2]);
   if(argc>3) n = atoi(argv[3]);

   printf("Matrix m: %d, k: %d, n: %d\n", m, k, n);

   // Initialisation
   LDA = m; LDY = m; LDX = k;

   //round the leading dimension up to a multiple of 32 for performance
   LDDA   = magma_roundup( m, 32 );
   LDDY   = magma_roundup( m, 32 );
   LDDX   = magma_roundup( k, 32 );

   // Set the seed for random numbers
   srand (time (NULL)); // For random numbers

   //mkl_set_num_threads(P);

   // Allocate memory
   if(!(A     =	(double *)	malloc(m*k*sizeof(double))))    magmatuto_abort("Memory allocation failed for A");
   if(!(Y     =	(double *)	malloc(m*n*sizeof(double)))) magmatuto_abort("Memory allocation failed for Y");
   if(!(X     =	(double *)	malloc(k*n*sizeof(double)))) magmatuto_abort("Memory allocation failed for X");
   if(!(YWORK = (double *)	malloc(m*n*sizeof(double))))    magmatuto_abort("Memory allocation failed for YWORK");

   //Allocate memory for the GPU
   merr = magma_dmalloc( &dA,  LDDA*k); magmatuto_magma_checkError(merr, "magma_dmalloc for dA");
   merr = magma_dmalloc( &dY,  LDDY*n); magmatuto_magma_checkError(merr, "magma_dmalloc for dY");
   merr = magma_dmalloc( &dX,  LDDX*n); magmatuto_magma_checkError(merr, "magma_dmalloc for dX");

   /*
    * Generating the problem, assuming the matrix is stored column by column
    */

   // Generate A
   magmatuto_stepPrint("Generating A");
   for(i=0;i<m*k;i++) {
     A[i] = (double) rand()/RAND_MAX ;
   }

   // Generate X
   for(i=0;i<k*n;i++) {
     X[i] = 1.0;
   }

   // Compute  Y = A*X  and store in Y.
   magmatuto_stepPrint("Computing Y");

   dgemm("N","N",&m,&n,&k,&dONE,A,&LDA,X,&LDX,&dZERO,Y,&LDY); /* Y = 1*A*X + 0*Y */

   /*
    *  Compute Compute the C = AX using MAGMA_DGEMM.
    */

    //Create a queue on the default device
 	 magma_int_t device = 0;
 	 magma_getdevice( &device );
 	 magma_queue_create(device, &queue);

    // Copy the matrices on the GPU
 	 magmatuto_stepPrint("Copying the matrices to the GPU");
 	 /* ... FILL HERE ... */
   /* ... FILL HERE ... */

   magmatuto_stepPrint("Calling MAGMA DGEMM");

   // Y = 1.0 * AX + 0 * Y

   /* ... FILL HERE ... */

   //Get the matrix on the CPU
   magmatuto_stepPrint("Copying the matrix from the GPU");
   /* ... FILL HERE ... */

   /*
    *  Check the result
    */
   magmatuto_stepPrint("Checking results");

   double resid = 0.0;

   for(i=0;i<m*n;i++) {
     resid += (YWORK[i] - Y[i]) * (YWORK[i] - Y[i]);
   }
   resid = sqrt(resid);

   printf("Residual: %e\n", resid);

   //Free memory
   magmatuto_stepPrint("Free memory");
   free(A);free(Y);free(X);
   free(YWORK);

   //free GPU memory and destroy the default queue
   magma_queue_destroy( queue );
   magma_free( dA );
   magma_free( dY );
   magma_free( dX );

   //Finalize MAGMA
   magma_finalize();

   return 0;
}
