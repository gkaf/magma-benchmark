#include <stdio.h>
#include <stdlib.h>

#include "magma_v2.h"

/* Check the error for a routine called by MAGMA */
void magmatuto_magma_checkError(magma_int_t const err, char const* const s)
{
	if (err != MAGMA_SUCCESS) {
		printf("ERROR: MAGMA check failed for %s, error: %d\n", s, err);
		exit(1);
	}
}
