/*
 *	Original author: Simplice Donfack
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>

#include "magma_v2.h"
#include "magmatuto_utils.h"

#ifndef ABS
#define ABS(a) (((a) < 0) ? -(a) : (a))
#endif

#ifndef MIN
#define MIN(a,b) ((a < b) ? a : b)
#endif

#ifndef MAX
#define MAX(a,b) ((a > b) ? a : b)
#endif

/* Abort the program execution */
void magmatuto_abort(char const* const s)
{
	printf("ABORT: %s",s);
	exit(1);
}

void magmatuto_return(char const* const func, int const line, char const* const file)
{
	fprintf(stderr, "Returning from '%s' in line %d of file '%s'\n", func, line, file);
}

void magmatuto_error(char const* const message, char const* const func, int const line, char const* const file)
{
	fprintf(stderr, "Error in '%s' in line %d of file '%s': ", func, line, file);
	fprintf(stderr, "%s\n", message);
}

/* Memory allocation processes */
mt_out magmatuto_malloc(void** const p, size_t const n, size_t const size)
{
	if (p == NULL) mt_check_return_error(MT_NULL_POINTER, "NULL pointer provided in argument 1");

	void* pt = malloc(n*size);

	if (pt == NULL) mt_check_return_error(MT_MALLOC_FAILURE, "'malloc/1' failed to allocate the requested amount of memory");

	*p = pt;

	return MT_SUCCESS;
}

mt_out magmatuto_calloc(void** const p, size_t const n, size_t const size)
{
	if (p == NULL) mt_check_return_error(MT_NULL_POINTER, "NULL pointer provided in argument 1");

	void* pt = calloc(n,size);

	if (pt == NULL) mt_check_return_error(MT_MALLOC_FAILURE, "'calloc/2' failed to allocate the requested amount of memory");

	*p = pt;

	return MT_SUCCESS;
}

mt_out magmatuto_free(void** const p)
{
	if (p == NULL) mt_check_return_error(MT_NULL_POINTER, "NULL pointer provided in argument 1");

	if (*p != NULL) {
		free(*p);
		*p = NULL;
	}

	return MT_SUCCESS;
}

/* Write a data in a file */
mt_out magmatuto_write(char const* const filename, char const* const data)
{
	FILE* const f = fopen(filename, "a+");
	
	if (f == NULL) {
		fprintf(stderr, "*** While acessing the file: %s\n", filename);
		mt_check_return_error(MT_FILE_ACESS_FAILURE, "Unable to open file");
	}

	fprintf(f, "%s", data);

	fclose(f);

	return MT_SUCCESS;
}

/*norm 1 or infinite of a matrice, type=1,I*/
double magmatuto_norm(char const type, int const M, int const N, double const* const A, int const LDA)
{
	double norm = 0.0;
	
	if (type == '1') {
		for (int j=0; j<N; ++j) {
			double sum = 0.0;
			for (int i=0; i<N; ++i) {
				sum += ABS(A[j*LDA+i]);
			}

			norm = MAX(norm,sum);
		}
	} else {
		for (int i=0; i<M; ++i) {
			double sum = 0.0;
			for (int j=0; j<N; ++j) {
				sum += ABS(A[j*LDA+i]);
			}
		norm = MAX(norm, sum);
		}
	}

	return norm;
}

/*norm 2 of a matrix*/
double magmatuto_norm2(int const M, int const N, double const* const A, int const LDA)
{
	double norm = 0.0;
	
	for(int i=0; i<M; ++i) {
		for(int j=0; j<N; ++j) {
			norm += A[j*LDA+i]*A[j*LDA+i];
		}
	}

	norm = sqrt(norm);

	return norm;
}

/* Print a matrix */
void magmatuto_printMat(int const M, int const N, double const* const A, int const LDA,
	char const* const desc)
{
	printf("Matrix: %s M:%d N:%d\n", desc, M, N);
	for (int i=0; i<M; ++i){
		for (int j=0; j<N; ++j){
			printf("%.2f ", A[j*LDA+i]);
		}
		printf("\n");
	}
}

/* Compute the residual
  eps: machine precision,
  norm1 = norm(b-AX),
  norm2 = norm(b-AX)/(eps*norm(A)*norm(X)*n) */
void magmatuto_residual(int m, int n, int nrhs,
	double *A, int LDA,
	double *B, int LDB,
	double *X, int LDX,
	double *eps, double *resid1, double *resid2)
{
	double Anorm, Rnorm, Xnorm;
	//double dwork[1];
	double dONE=1.0, dMONE=-1.0;
	//eps = dlamch("E");//
	*eps = 1/pow(2,53);//

	/*    Compute  B - A*X  and store in B. */
	dgemm("N", "N", &m, &nrhs, &n, &dMONE, A, &LDA, X, &LDX, &dONE, B, &LDB); // B = -1*A*X + 1*B

	/*
	Anorm = dlange_( "F", &m, &n, A, &LDA, dwork );
	Rnorm = dlange( "F", &n, &nrhs, B, &LDB, dwork );
	Xnorm = dlange( "F", &n, &nrhs, X, &LDX, dwork );
	*/
	Anorm = magmatuto_norm2(m, n, A, LDA);
	Rnorm = magmatuto_norm2(n, nrhs, B, LDB);
	Xnorm = magmatuto_norm2(n, nrhs, X, LDX);

	*resid1 = Rnorm;
	*resid2 = Rnorm/(n * Anorm * Xnorm * (*eps));

	//printf("Machine precision: %e\nnorm(A): %.2e, norm(X): %.2e, norm(B-AX): %.2e, norm(B-AX)/(norm(A)*n*eps): %.2e \n", eps, Anorm, Xnorm, Rnorm, resid);
	//printf("Machine precision: %e, \nnorm(B-AX): %.2e\n", eps);
}

/* Print a progress step message */
void magmatuto_stepPrint(char const* const s)
{
	printf("%s ........\n",s);
}

/* Get the current time of the dat in microseconds */
long magmatuto_usecs ()
{
	long const us_p_s = 1000000;

	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec*us_p_s + t.tv_usec;
}
