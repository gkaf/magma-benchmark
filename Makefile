include make.inc

define set_config_build_dir
$(if $(or $(filter $(2), build), $(filter $(2), debug)),
	$(eval $(1):=$(2)),
	$(error Unsuported target: $(2))
)
endef

define set_config_var
$(if $(filter $(1), build),$(eval $(2):=$(3)))
$(if $(filter $(1), debug),$(eval $(2):=$(4)))
endef

ifndef CONFIG
CONFIG:=build
endif

## Build directory setup

$(call set_config_build_dir,BUILD_DIR,$(CONFIG))

OBJ_DIR := $(BUILD_DIR)/obj
BIN_DIR := $(BUILD_DIR)/bin
LIB_DIR := $(BUILD_DIR)/lib

HEADER_DIR := include

SRC_DIR := src
UTIL_DIR := $(SRC_DIR)/utils

TUTORIAL_SRC_DIR := $(SRC_DIR)/tutorial
SOLUTION_SRC_DIR := $(SRC_DIR)/solutions
TUTORIAL_OBJ_DIR := $(OBJ_DIR)/tutorial
SOLUTION_OBJ_DIR := $(OBJ_DIR)/solutions
TUTORIAL_BIN_DIR := $(BIN_DIR)/tutorial
SOLUTION_BIN_DIR := $(BIN_DIR)/solutions

## Compiler setup

CC := gcc
CFLAGS_BUILD := -fopenmp -O3 -W -Wall -funroll-loops
CFLAGS_DEBUG := -fopenmp -W -Wall -g
$(call set_config_var,$(CONFIG),CFLAGS,$(CFLAGS_BUILD),$(CFLAGS_DEBUG))

CFORTRAN := gfortran
FFLAGS_BUILD := -O3
FFLAGS_DEBUG := -g
$(call set_config_var,$(CONFIG),FFLAGS,$(FFLAGS_BUILD),$(FFLAGS_DEBUG))

LINKER := $(CC)
LFLAGS_BUILD := -Wl,-rpath=$(MAGMA_LIB):$(CUDA_LIB):$(LAPACK_LIB)
LFLAGS_DEBUG := $(LFLAGS_BUILD)
$(call set_config_var,$(CONFIG),LFLAGS,$(LFLAGS_BUILD),$(LFLAGS_DEBUG))

# The archiver and the flag(s) to use when building archive (library)
ARCH := ar
ARCHFLAGS := cr
RANLIB := echo
RANLIB_PATH := $(shell which ranlib)
ifdef RANLIB_PATH
	RANLIB := ranlib
endif

# C preprocessor defs for compilation for the Fortran interface
# (-DNoChange, -DAdd_, -DUpCase, or -DAdd__)
CDEFS := -DAdd_

## Setup for the auxiliary MAGMATUTO library

# Better to list sources explicitly
#UTIL_SOURCES = $(filter %.c, $(wildcard $(UTIL_DIR)/*))
#UTIL_SOURCES = $(filter %.c, $(UTIL_SOURCE_LIST))

UTIL_SOURCE_LIST := magmatuto_magma_utils.c magmatuto_utils.c
UTIL_SOURCES := $(patsubst %, $(UTIL_DIR)/%, $(UTIL_SOURCE_LIST))
UTIL_OBJECTS := $(patsubst $(UTIL_DIR)/%.c, $(OBJ_DIR)/%.o, $(UTIL_SOURCES))

UTIL_HEADER_LIST := magmatuto_magma_utils.h magmatuto_utils.h
UTIL_HEADERS := $(patsubst %, $(HEADER_DIR)/%, $(UTIL_HEADER_LIST))

# Prepare libraries

## Where to output the compiled MAGMATUTO lib (no need to change)
UTIL_LIB := $(LIB_DIR)
LUTIL := magmatuto
UTIL_LIB_ARCH := $(UTIL_LIB)/lib$(LUTIL).a

## Library inclusions

INCLUDE_PATH_LIST := $(HEADER_DIR) $(MAGMA_INCLUDE) $(CUDA_INCLUDE)

INCLUDE_PATHS := $(patsubst %, -I%, $(INCLUDE_PATH_LIST))

## Library linking

LIB_LIST := $(UTIL_LIB) $(MAGMA_LIB) $(CUDA_LIB) $(LAPACK_LIB) $(BLAS_LIB) $(SYS_LIB)
L_LIST := $(LUTIL) $(LMAGMA) $(LCUDA) $(LLAPACK) $(LBLAS) $(LSYS)

LIB_PATHS := $(patsubst %, -L%, $(LIB_LIST))
LIB_LINKS := $(patsubst %, -l%, $(L_LIST))

# Note: Ensures the default behavior for all libraries that follow (redundant).
#       Recomended by MKL for its libraries, added here for all libraries.
#       May be overwriten for other user defined libraries
LIB_LINKS_FLAGS := -Wl,--no-as-needed

## Setup for the tutorial code

TUTORIAL_SOURCE_LIST := \
	tutorial1_test_dgetrf.c \
	tutorial2_magma_dgetrf.c \
	tutorial3_magma_dgetrf_pinned.c \
	tutorial4_magma_dgetrf_gpu.c \
	tutorial5_magma_dgetrf_native.c \
	tutorial6_magma_blas.c \
	tutorial7_magma_blas_eventrecord.c\
	tutorial8_magma_dgetrf_mgpu.c

TUTORIAL_SOURCES := $(patsubst %, $(TUTORIAL_SRC_DIR)/%, $(TUTORIAL_SOURCE_LIST))
TUTORIAL_OBJECTS := $(patsubst $(TUTORIAL_SRC_DIR)/%.c, $(TUTORIAL_OBJ_DIR)/%.o, $(TUTORIAL_SOURCES))
TUTORIAL_BINARIES := $(patsubst $(TUTORIAL_OBJ_DIR)/%.o, $(TUTORIAL_BIN_DIR)/%, $(TUTORIAL_OBJECTS))

SOLUTION_SOURCES := $(patsubst %, $(SOLUTION_SRC_DIR)/%, $(TUTORIAL_SOURCE_LIST))
SOLUTION_OBJECTS := $(patsubst $(SOLUTION_SRC_DIR)/%.c, $(SOLUTION_OBJ_DIR)/%.o, $(SOLUTION_SOURCES))
SOLUTION_BINARIES := $(patsubst $(SOLUTION_OBJ_DIR)/%.o, $(SOLUTION_BIN_DIR)/%, $(SOLUTION_OBJECTS))

# Targets

.PHONY: all
all: prepare tutorial solutions

.PHONY: tutorial
tutorial: $(UTIL_LIB_ARCH) $(TUTORIAL_BINARIES)

.PHONY: solutions
solutions: $(UTIL_LIB_ARCH) $(SOLUTION_BINARIES)

.PHONY: util_lib
util_lib: $(UTIL_LIB_ARCH)

## Compilation
$(TUTORIAL_OBJECTS): $(TUTORIAL_OBJ_DIR)/%.o: $(TUTORIAL_SRC_DIR)/%.c $(UTIL_HEADERS) | $(TUTORIAL_OBJ_DIR)
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_PATHS)

$(TUTORIAL_BINARIES): $(TUTORIAL_BIN_DIR)/%: $(TUTORIAL_OBJ_DIR)/%.o $(UTIL_LIB_ARCH) | $(TUTORIAL_BIN_DIR)
	$(LINKER) -o $@ $^ $(LFLAGS) $(LIB_PATHS) $(LIB_LINKS_FLAGS) $(LIB_LINKS)

$(SOLUTION_OBJECTS): $(SOLUTION_OBJ_DIR)/%.o: $(SOLUTION_SRC_DIR)/%.c $(UTIL_HEADERS) | $(SOLUTION_OBJ_DIR)
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_PATHS)

$(SOLUTION_BINARIES): $(SOLUTION_BIN_DIR)/%: $(SOLUTION_OBJ_DIR)/%.o $(UTIL_LIB_ARCH) | $(SOLUTION_BIN_DIR)
	$(LINKER) -o $@ $^ $(LFLAGS) $(LIB_PATHS) $(LIB_LINKS_FLAGS) $(LIB_LINKS)

$(UTIL_OBJECTS): $(OBJ_DIR)/%.o: $(UTIL_DIR)/%.c $(UTIL_HEADERS) | $(OBJ_DIR)
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_PATHS)

$(UTIL_LIB_ARCH): $(UTIL_OBJECTS) | $(LIB_DIR)
	$(ARCH) $(ARCHFLAGS) $(UTIL_LIB_ARCH) $(UTIL_OBJECTS)
	$(RANLIB) $(UTIL_LIB_ARCH)

## Housekeeping

TUTORIAL_DIRS := \
	$(TUTORIAL_OBJ_DIR) \
	$(SOLUTION_OBJ_DIR) \
	$(TUTORIAL_BIN_DIR) \
	$(SOLUTION_BIN_DIR)

.PHONY: prepare
prepare: $(OBJ_DIR) $(BIN_DIR) $(LIB_DIR) $(TUTORIAL_DIRS)
	@{ \
		if [ -z $(RANLIB_PATH) ]; then \
			echo "Warning, no ranlib installed, using: ranlib = $(RANLIB)"; \
		fi; \
	}

$(OBJ_DIR) $(BIN_DIR) $(LIB_DIR) : % : | $(BUILD_DIR)
	mkdir $@

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(TUTORIAL_OBJ_DIR) $(SOLUTION_OBJ_DIR) : % : | $(OBJ_DIR)
	mkdir $@

$(TUTORIAL_BIN_DIR) $(SOLUTION_BIN_DIR) : % : | $(BIN_DIR)
	mkdir $@

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)
