CONDA_HOME := /home/gkaf/.conda/pkgs

# Link with MAGMA

MAGMA_ROOT := /home/gkaf/lib/MAGMA
MAGMA_INCLUDE := $(MAGMA_ROOT)/include
MAGMA_LIB := $(MAGMA_ROOT)/lib
LMAGMA := magma

# Link with CUDA

CUDA_ROOT := /usr/local/cuda
CUDA_INCLUDE := $(CUDA_ROOT)/include

## Note that some distributions require: cuda cublasLt
CUDA_LIB := $(CUDA_ROOT)/lib64
LCUDA := cudart cusparse cublas

# Link with BLAS/LAPACK or MKL

MKL_ROOT := $(CONDA_HOME)/mkl-2022.0.1-h8d4b97c_803
LAPACK_LIB := $(MKL_ROOT)/lib
LLAPACK := mkl_intel_lp64 mkl_gf_lp64 mkl_gnu_thread mkl_core

# Link with BLAS (ignore if BLAS is included in your lapack library, e.g. MKL)

BLAS_LIB :=
LBLAS :=

# Link with system libraries

SYS_LIB :=
LSYS := gomp pthread m
